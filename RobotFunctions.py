import random
from gasp import *


class Player:
    pass

class Robot:
    pass

def random_between(a,b):
    return random.randint(a,b)


def place_robot2():
    global robot_x2
    global robot_y2
    global robot_shape2
    robot = Robot()
    robot_x2 = int(random_between(0,620))
    robot_y2 = int(random_between(0,460))
    robot_shape2 = Circle((robot_x2, robot_y2), 5, filled=True)


def place_robot3():
    global robot_x3
    global robot_y3
    global robot_shape3
    robot = Robot()
    robot_x3 = int(random_between(0,620))
    robot_y3 = int(random_between(0,460))
    robot_shape3 = Circle((robot_x3, robot_y3), 5, filled=True)


def move_robot():
    global robot_x, robot_y, robot_shape

    if robot_x > player_x:
        robot_x -= 7
    elif robot_x < player_x:
        robot_x += 7
    if robot_y-0 > player_y:
        robot_y -= 7
    elif robot_y-0 < player_y:
        robot_y += 7
    move_to(robot_shape, (robot_x, robot_y))

def move_robot2():
    global robot_x2, robot_y2, robot_shape2

    if robot_x2 > player_x:
        robot_x2 -= 7
    elif robot_x2 < player_x:
        robot_x2 += 7
    if robot_y2-0 > player_y:
        robot_y2 -= 7
    elif robot_y2-0 < player_y:
        robot_y2 += 7
    move_to(robot_shape2, (robot_x2, robot_y2))


def move_robot3():
    global robot_x3, robot_y3, robot_shape3

    if robot_x3 > player_x:
        robot_x3 -= 7
    elif robot_x3 < player_x:
        robot_x3 += 7
    if robot_y3-0 > player_y:
        robot_y3 -= 7
    elif robot_y3-0 < player_y:
        robot_y3 += 7
    move_to(robot_shape3, (robot_x3, robot_y3))


def place_player():
    global player_x
    global player_y
    global player_shape
    player = Player()
    player_x = 620#int(random_between(0,620))
    player_y = 240#int(random_between(0,460))
    player_shape = Circle((player_x, player_y), 5)


def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    if key == 'w':
        player_y += 10
    elif key == 'a':
        player_x -= 10
    elif key == 's':
        player_y -= 10
    elif key == 'd':
        player_x += 10
    elif key == 'c':
        player_x = 320
        player_y = 240

    move_to(player_shape, (player_x, player_y))


def place_robot():
    global robot_x
    global robot_y
    global robot_shape
    robot = Robot()
    robot_x = int(random_between(0,620))
    robot_y = int(random_between(0,460))
    robot_shape = Circle((robot_x, robot_y), 5, filled=True)
