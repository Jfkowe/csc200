from gasp import *
from random import randint
from gasp.utils import read_yesorno


# Class that runs everything
class RunGame:
    # Defining key variables
    LEFT = "a"
    RIGHT = "d"
    UP = "w"
    DOWN = "s"
    UPRIGHT = "9"
    UPLEFT = "7"
    DOWNRIGHT = "3"
    DOWNLEFT = "1"
    TELEPORT = "h"
    TOP = 46
    BOTTOM = 0
    LEFT_BOARDER = 1
    RIGHT_BOARDER = 62
    # Used to make sure the player hits the correct keys in move player
    INPUT_LIST = ["a", "d", "w", "s", "9", "7", "3", "1", "h"]
    robot_check = []
    robot_check2 = []
    teleport_count = 0

    def __init__(self):
        numbot = 0

        while numbot < 2:
            try:
                numbot = int(input("How many robots? "))
            except:
                numbot = int(input(f"Please enter a number.\nHow many robots would you like to play against? (Min 2 Robots) "))

        RunGame.teleport_count = numbot
        begin_graphics()
        self.finished = False
        self.robots = [Robot() for i in range(numbot)]
        robot_check2 = self.robots
        self.player = Player()
        self.junk = []

    def next_move(self):
        self.check_collisions()

        if self.finished == False:
            self.player.move()

            for bot in self.robots:
                bot.move(self.player)

    def check_collisions(self):
        # Creates temporary variable and clears the Robots list
        alive_robots = self.robots
        self.robots = []


        for bot in alive_robots:
            # Checks if a Robot has hit the Player
            if bot.x == self.player.x and bot.y == self.player.y:
                self.finished = True
                Text("Loser Alert! You Lose", (320, 240), size=400)
                sleep(3)


            for thing in alive_robots:
                if bot == thing:
                    continue
                if bot.x == thing.x and bot.y == thing.y:
                    self.junk.append(bot)
                    remove_from_screen(bot.shape)
                    bot.shape = Circle((10 * bot.x, 10 * bot.y), 5, filled=True)


            for thing in self.junk:
                if bot.x == thing.x and bot.y == thing.y and (bot in self.junk) == False:
                    self.junk.append(bot)
                    remove_from_screen(bot.shape)


            if (bot in self.junk) == False and (bot in self.robots) == False:
                self.robots.append(bot)


        if len(self.robots) == 0:
            self.finished = True
            Text("You Are The Big Winner!", (320, 240), size=100)
            sleep(3)


    def over(self):
        end_graphics()



class Player:
    # Places player
    def __init__(self):
        self.safely_place()


    def place(self):
        self.x = randint(RunGame.LEFT_BOARDER, RunGame.RIGHT_BOARDER)
        self.y = randint(RunGame.BOTTOM, RunGame.TOP)


    def collided(self, thing):
        for item in thing:
            if self.x == item.x and self.y == item.y:
                return(True)
            else:
                return(False)


    def safely_place(self):
        self.place()

        while self.collided(RunGame.robot_check2):
            self.place()

        self.shape = Circle((10*self.x, 10*self.y), 5, filled=False)


    def teleport(self):
        # Doesn't let the player teleport if they are "out" of teleports
        if RunGame.teleport_count > 0:
            remove_from_screen(self.shape)
            self.safely_place()
            RunGame.teleport_count -= 1


    def move(self):
        key = update_when("key_pressed")

        while key not in RunGame.INPUT_LIST:
            key = update_when("key_pressed")

        while key == RunGame.TELEPORT:
            self.teleport()
            key = update_when("key_pressed")

            while key not in RunGame.INPUT_LIST:
                key = update_when("key_pressed")

        if key == RunGame.UP and self.y < RunGame.TOP:
            self.y += 1
        elif key == RunGame.RIGHT and self.x < RunGame.RIGHT_BOARDER:
            self.x += 1
        elif key == RunGame.DOWN and self.y > RunGame.BOTTOM:
            self.y -= 1
        elif key == RunGame.LEFT and self.x > RunGame.LEFT_BOARDER:
            self.x -= 1
        elif key == RunGame.UPRIGHT:
            if self.x < RunGame.RIGHT_BOARDER:
                self.x += 1
            if self.y < RunGame.TOP:
                self.y += 1
        elif key == RunGame.DOWNRIGHT:
            if self.x < RunGame.RIGHT_BOARDER:
                self.x += 1
            if self.y > RunGame.BOTTOM:
                self.y -= 1
        elif key == RunGame.DOWNLEFT:
            if self.x > RunGame.LEFT_BOARDER:
                self.x -= 1
            if self.y > RunGame.BOTTOM:
                self.y -= 1
        elif key == RunGame.UPLEFT:
            if self.x > RunGame.LEFT_BOARDER:
                self.x -= 1
            if self.y < RunGame.TOP:
                self.y += 1

        move_to(self.shape, (10*self.x, 10*self.y))



class Robot:

    def __init__(self):
        self.place()

    # Checks if the robot has collided with something
    def collided(self, thing):
        for item in thing:
            if self.x == item[0] and self.y == item[1]:
                return(True)
            else:
                return(False)


    def place(self):
        self.x = randint(RunGame.LEFT_BOARDER, RunGame.RIGHT_BOARDER)
        self.y = randint(RunGame.BOTTOM, RunGame.TOP)
        self.coords = [self.x, self.y]

        # Makes sure a robot won't spawn on another robot
        if not self.collided(RunGame.robot_check):
            self.shape = Box((10*self.x, 10*self.y), 10, 10)
            self.coords.append(RunGame.robot_check)


    def move(self, player):
        if self.x > player.x and self.y > player.y:
            self.x -= 1
            self.y -= 1
        elif self.x < player.x and self.y > player.y:
            self.x += 1
            self.y -= 1
        elif self.x < player.x and self.y < player.y:
            self.x += 1
            self.y += 1
        elif self.x > player.x and self.y < player.y:
            self.x -= 1
            self.y += 1
        elif self.x > player.x:
            self.x -= 1
        elif self.x < player.x:
            self.x += 1
        elif self.y > player.y:
            self.y -= 1
        elif self.y < player.y:
            self.y += 1

        move_to(self.shape, (10*self.x, 10*self.y))



game = RunGame()

# Loop to keep the game running
while not game.finished:
    game.next_move()

    # Seeing if the player wants to play again after winning or losing
    if game.finished:
        if read_yesorno("Would you like to play again? "):
            game = RunGame()

# Closes the game
game.over()
