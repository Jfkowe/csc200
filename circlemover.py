from gasp import *
import random

def random_between(a,b):
    return random.randint(a,b)


class JIMBO():
    global robot_x, robot_y, robot_shape
    robot_x = int(random_between(0,620))
    robot_y = int(random_between(0,460))
    robot_shape = Circle((robot_x, robot_y), 5, filled=True)

def make_jimbos():
    global robot_x, robot_y, robot_shape
    jimbo = JIMBO()
    jimbo.robot_shape


begin_graphics()
finished = False

while not finished:
    make_jimbos()
