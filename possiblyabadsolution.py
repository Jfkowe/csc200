import random
def create():
    for i in range (0,2):
        name = "Jack"
        x = 1
        filename = name + str(x) + ".py"
        f = open(filename, "w")
        f.write("def function(): \n")
        f.write("   print('hello')")
        f.close()
        x += 1




def make_robot():
    for i in range(0,2):
        robofile = "robofile"
        x = 1
        filename = robofile + str(x) + ".py"
        f = open(filename, "w")
        f.write("from gasp import *\n")
        f.write("import random\n")
        f.write("def random_between(a,b):\n")
        f.write("   return random.randint(a,b)\n")
        f.write("def place_robot2(): \n")
        f.write("   global robot_x2\n")
        f.write("   global robot_y2\n")
        f.write("   global robot_shape2\n")
        f.write("   robot_x2 = int(random_between(0,620))\n")
        f.write("   robot_y2 = int(random_between(0,460))\n")
        f.write("   robot_shape2 = Circle((robot_x2, robot_y2), 5, filled=True)")
        f.close()
