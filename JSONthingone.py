import json
x = 1
thingsIcansee = '''
[
    {
        "name" : "Bowl",
        "shape" : {
            "dimension" : "3D",
            "geometric" : "round"
            },
            "like" : "I like it"
    },
    {
        "name" : "pencil",
        "shape" : {
            "dimension" : "3D",
            "geometric" : "oblong"
        },
        "like" : "I don't like it"
    },
    {
        "name" : "dog",
        "shape" : {
            "dimension" : "3D",
            "geometric" : "doglike"
        },
        "like" : "I like it"
    }
]'''

lookaround = json.loads(thingsIcansee)
for item in lookaround:
    print('Object ', str(x), ' is: ', item["name"])
    print('Its dimensions are', item["shape"])
    print('and ', item["like"])
    print('---------')
    x += 1
