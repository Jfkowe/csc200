from gasp import *
import random
def random_between(a,b):
   return random.randint(a,b)
def place_robot2(): 
   global robot_x2
   global robot_y2
   global robot_shape2
   robot_x2 = int(random_between(0,620))
   robot_y2 = int(random_between(0,460))
   robot_shape2 = Circle((robot_x2, robot_y2), 5, filled=True)