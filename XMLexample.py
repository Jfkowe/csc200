import xml.etree.ElementTree as ET
data = """
<rolodex>
    <contacts>
        <contact>
            <name>Jack Kennedy</name>
            <phonenumber>1-800-JFK</phonenumber>
            <dog>Labradoodle</dog>
        </contact>
        <contact>
            <name>Jack Ploetz</name>
            <phonenumber>1-800-champ</phonenumber>
            <dog>No dog I don't think</dog>
        </contact>
        <contact>
            <name>Dog owning man</name>
            <phonenumber>1-800-DOGOWNER</phonenumber>
            <dog>Owns many dogs</dog>
        </contact>
        <contact>
            <name>Police Man</name>
            <phonenumber>911</phonenumber>
            <dog>K-9M</dog>
        </contact>
        <contact>
            <name>Operator</name>
            <phonenumber>0</phonenumber>
            <dog>No dog for sure</dog>
        </contact>
    </contacts>
</rolodex>
"""

tree = ET.fromstring(data)
lst = tree.findall('contacts/contact')
print("Number of people in rolodex: ", len(lst))
for item in lst:
    print("Name: ", item.find('name').text)
    print("Phone Number: ", item.find('phonenumber').text)
    print("Dog ownership status: ", item.find('dog').text)
    print('-------------------------')
#print('Name: ', tree.find('nametwo').text)
