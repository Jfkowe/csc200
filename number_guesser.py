import random
import time
def give_num():
    global guess
    try:
        guess = int(input("Make a guess: "))
    except:
        print("That wasn't a number")



def game():
    global guess
    print("OK, I've thought of a number between 1 and 1000.")
    number = random.randint(1,1000)
    #print(number)
    guesses = 0
    give_num()
    while guess != number:
        if guess > number:
            time.sleep(0.5)
            print("That's too high.")
        if guess < number:
            time.sleep(0.5)
            print("That's too low.")
        guesses += 1
        give_num()
    time.sleep(0.5)
    print("That was my number. Well done!")
    time.sleep(0.5)
    print(f"You took {guesses+1} guesses.")
    time.sleep(0.5)
    another = str(input("Should we play again (yes or no)? "))
    if another == "yes":
        game()
game()
